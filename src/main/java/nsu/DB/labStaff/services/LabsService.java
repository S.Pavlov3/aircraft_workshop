package nsu.DB.labStaff.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.labStaff.model.Labs;
import nsu.DB.labStaff.repositories.LabsRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LabsService extends AbstractCrudService<Labs> {
    public LabsService(LabsRepository repository){ this.setRepository(repository);}

    public Page<Labs> findByProduct(Integer productId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((LabsRepository)getRepository()).findByProduct(productId, pageable);
    }
}
