package nsu.DB.labStaff.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.labStaff.model.LabEquipment;
import nsu.DB.labStaff.repositories.LabEquipmentRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LabEquipmentService extends AbstractCrudService<LabEquipment> {
    public LabEquipmentService(LabEquipmentRepository repository){ this.setRepository(repository);}

    public Page<LabEquipment> findByLabAndProductOrProductTypeAndProductTestingDate(
            Date startDate, Date endDate, String type, Integer productId, Integer labId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((LabEquipmentRepository)getRepository()).
                findByLabAndProductOrProductTypeAndProductTestingDate(startDate, endDate, type, productId,labId, pageable);
    }
}
