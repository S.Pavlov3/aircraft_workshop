package nsu.DB.labStaff.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.labStaff.model.ProductTesting;
import nsu.DB.labStaff.repositories.ProductTestingRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductTestingService extends AbstractCrudService<ProductTesting> {
    public ProductTestingService(ProductTestingRepository repository){ this.setRepository(repository);}
}
