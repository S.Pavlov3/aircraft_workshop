package nsu.DB.labStaff.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.labStaff.model.ProductTesting;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProductTestingDto extends Dto<ProductTesting> {
    
    private Integer productId;
    private Integer testerId;
    private Integer labEquipmentId;

    private Date startDate;
    private Date endDate;
}
