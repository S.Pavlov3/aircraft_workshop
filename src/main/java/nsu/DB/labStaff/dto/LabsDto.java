package nsu.DB.labStaff.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.labStaff.model.Labs;

@Getter
@Setter
@NoArgsConstructor
public class LabsDto extends Dto<Labs> {
    
    private String name;
}
