package nsu.DB.labStaff.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.labStaff.model.LabEquipment;

@Getter
@Setter
@NoArgsConstructor
public class LabEquipmentDto extends Dto<LabEquipment> {

    private Integer labId;
}
