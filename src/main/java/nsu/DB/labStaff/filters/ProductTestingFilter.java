package nsu.DB.labStaff.filters;

import nsu.DB.labStaff.model.ProductTesting;
import nsu.DB.utils.filters.Filter;

public class ProductTestingFilter implements Filter<ProductTesting> {
    public int productId;
    public int testerId;
    public int labEquipmentId;
}
