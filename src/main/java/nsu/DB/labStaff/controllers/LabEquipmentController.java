package nsu.DB.labStaff.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.labStaff.dto.LabEquipmentDto;
import nsu.DB.labStaff.mappers.LabEquipmentMapper;
import nsu.DB.labStaff.model.LabEquipment;
import nsu.DB.labStaff.services.LabEquipmentService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(AppPath.PATH_V1+"/labEquipment")
public class LabEquipmentController extends AbstractCrudController<LabEquipment, LabEquipmentDto> {

    public LabEquipmentController(LabEquipmentService service, LabEquipmentMapper mapper) {
        super(service, mapper, "LabEquipment");
    }

/*
    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(LabEquipmentFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }
*/

    /**
     *Запрос 13:
     * Состав оборудования, использованного при испытании определнного изделия, категории изделия
     * и в некторой лаборатории за определенный промежуток времени
     */
    /**
     *НЕ ПРОТЕСТИРОВАННЫЙ КРУД С ДАТОЙ
     */
    @GetMapping("by-lab-and-product-or-product-type-and-product-testing-date")
    public PageDto<?> findByProductTypeAndProductTestingDate(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date startDate,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date endDate,
            @RequestParam(required = false)
            String type,
            @RequestParam(required = false)
            Integer productId, Integer labId, FindRequestParameters parameters){
        Page<LabEquipment> page = ((LabEquipmentService)getService()).
                findByLabAndProductOrProductTypeAndProductTestingDate(startDate, endDate, type, productId, labId,parameters);
    return getMapper().toPageDto(page);
}
}
