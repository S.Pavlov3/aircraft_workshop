package nsu.DB.labStaff.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.labStaff.dto.LabsDto;
import nsu.DB.labStaff.mappers.LabsMapper;
import nsu.DB.labStaff.model.Labs;
import nsu.DB.labStaff.services.LabsService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/labs")
public class LabsController extends AbstractCrudController<Labs, LabsDto> {

    public LabsController(LabsService service, LabsMapper mapper) {
        super(service, mapper, "Lab");
    }

/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(LabsFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     *Запрос 10:
     * Перечень лабораторя, участвоваших в испытани изделия
     */
    @GetMapping("by-product")
    public PageDto<?> getByProduct(Integer productId, FindRequestParameters parameters) {
        LabsService labsService = ((LabsService) getService());
        Page<Labs> page = labsService.findByProduct(productId, parameters);
        return getMapper().toPageDto(page);
    }
}
