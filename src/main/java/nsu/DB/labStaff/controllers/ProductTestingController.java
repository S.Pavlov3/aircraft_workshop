package nsu.DB.labStaff.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.labStaff.dto.ProductTestingDto;
import nsu.DB.labStaff.mappers.ProductTestingMapper;
import nsu.DB.labStaff.model.ProductTesting;
import nsu.DB.labStaff.services.ProductTestingService;
import nsu.DB.utils.AppPath;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/productTesting")
public class ProductTestingController extends 
        AbstractCrudController<ProductTesting, ProductTestingDto> {

    public ProductTestingController(ProductTestingService service, ProductTestingMapper mapper) {
        super(service, mapper, "ProductTesting");
    }

 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(ProductTestingFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
}
