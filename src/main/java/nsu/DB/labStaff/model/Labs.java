package nsu.DB.labStaff.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "Labs")
public class Labs extends Identifiable {
    
    private String name;
}
