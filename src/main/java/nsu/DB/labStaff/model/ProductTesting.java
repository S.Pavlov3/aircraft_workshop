package nsu.DB.labStaff.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.employees.model.Testers;
import nsu.DB.products.model.Products;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Setter
@Getter
@Table(name = "ProductTesting")
public class ProductTesting extends Identifiable {
    
    @ManyToOne
    @JoinColumn(name = "productId",referencedColumnName = "id")
    private Products product;
    
    @ManyToOne
    @JoinColumn(name = "testerId",referencedColumnName = "id")
    private Testers tester;

    @ManyToOne
    @JoinColumn(name = "labEquipmentId",referencedColumnName = "id")
    private LabEquipment labEquipment;

    private Date startDate;

    private Date endDate;
}
