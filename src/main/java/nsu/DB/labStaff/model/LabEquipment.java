package nsu.DB.labStaff.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "LabEquipment")
public class LabEquipment extends Identifiable {
    
    @ManyToOne
    @JoinColumn(name = "labId",referencedColumnName = "id")
    private Labs lab;
}
