package nsu.DB.labStaff.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.labStaff.dto.LabEquipmentDto;
import nsu.DB.labStaff.model.LabEquipment;
import nsu.DB.labStaff.services.LabsService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LabEquipmentMapper extends Mapper<LabEquipment, LabEquipmentDto> {

    private final LabsService labsService;
    
    private final ModelMapper modelMapper;

    @Override
    public LabEquipment toEntity(LabEquipmentDto dto) throws NoDataFoundException {
        LabEquipment entity = modelMapper.map(dto, LabEquipment.class);
        Integer labId = dto.getLabId();
        if (labId != null) {
            entity.setLab(labsService.getEntity(labId));
        }
        else{
            throw new NoDataFoundException("Wrong labId");
        }
        return entity;
    }

    @Override
    public LabEquipmentDto toDto(LabEquipment entity) {
        LabEquipmentDto dto = modelMapper.map(entity, LabEquipmentDto.class);
        dto.setLabId(entity.getLab()!=null ? entity.getLab().getId() : null);
        return dto;
    }
}
