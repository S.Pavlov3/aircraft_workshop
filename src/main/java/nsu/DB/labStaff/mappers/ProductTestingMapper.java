package nsu.DB.labStaff.mappers;


import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.employees.services.TestersService;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.labStaff.dto.ProductTestingDto;
import nsu.DB.labStaff.model.ProductTesting;
import nsu.DB.labStaff.services.LabEquipmentService;
import nsu.DB.products.services.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductTestingMapper extends Mapper<ProductTesting, ProductTestingDto> {
    
    private final LabEquipmentService labEquipmentService;

    private final ProductsService productsService;

    private final TestersService testersService;

    private final ModelMapper modelMapper;

    @Override
    public ProductTesting toEntity(ProductTestingDto dto) throws NoDataFoundException {
        ProductTesting entity = modelMapper.map(dto, ProductTesting.class);
        if(dto.getLabEquipmentId() !=null)
            entity.setLabEquipment(labEquipmentService.getEntity(dto.getLabEquipmentId()));
        else throw new NoDataFoundException("Wrong labEquipmentId");
        if(dto.getProductId()!=null)
            entity.setProduct(productsService.getEntity(dto.getProductId()));
        else throw new NoDataFoundException("Wrong productId");
        if(dto.getTesterId()!=null)
            entity.setTester(testersService.getEntity(dto.getTesterId()));
        else throw new NoDataFoundException("Wrong testerId");
        if(dto.getEndDate()== null || dto.getStartDate()==null )
            throw new NoDataFoundException("Wrong dates");
        entity.setEndDate(dto.getEndDate());
        entity.setStartDate(dto.getStartDate());
        return entity;
    }

    @Override
    public ProductTestingDto toDto(ProductTesting entity) {
        ProductTestingDto dto = modelMapper.map(entity,ProductTestingDto.class);
        dto.setLabEquipmentId(entity.getLabEquipment()!= null ? entity.getLabEquipment().getId() : null);
        dto.setProductId(entity.getProduct() != null ? entity.getProduct().getId() : null);
        dto.setTesterId(entity.getTester() != null ? entity.getTester().getId() : null);
        entity.setEndDate(dto.getEndDate());
        entity.setStartDate(dto.getStartDate());
        return dto;
    }
}
