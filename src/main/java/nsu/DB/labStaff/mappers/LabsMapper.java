package nsu.DB.labStaff.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.labStaff.dto.LabsDto;
import nsu.DB.labStaff.model.Labs;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LabsMapper extends Mapper<Labs, LabsDto> {

    private final ModelMapper modelMapper;

    @Override
    public Labs toEntity(LabsDto dto) {
        Labs entity = modelMapper.map(dto,Labs.class);
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public LabsDto toDto(Labs entity) {
        LabsDto dto = modelMapper.map(entity, LabsDto.class);
        dto.setName(entity.getName());
        return dto;
    }
}
