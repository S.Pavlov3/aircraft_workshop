package nsu.DB.labStaff.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.labStaff.model.ProductTesting;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductTestingRepository extends JpaFilterRepository<ProductTesting,Integer> {

    /*@Query(
            "SELECT pt" +
                    "FROM productTesting" +
                    "WHERE :#{#filter.productId} is null or pt.product.id = :#{#filter.productId}" +
                    "and :#{#filter.testerID} is null or pt.tester.id = :#{#filter.testerId}" +
                    "and :#{#filter.labEquipmentId} is null or pt.labEquipment.id = :#{#filter.labEquipmentId}"
    )
    @Override
    Page<ProductTesting> findAllByFilter(@Param("filter")Filter<ProductTesting> filter, Pageable pageable);
*/}

