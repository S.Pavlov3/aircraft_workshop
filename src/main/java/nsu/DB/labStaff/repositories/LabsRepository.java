package nsu.DB.labStaff.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.labStaff.model.Labs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LabsRepository extends JpaFilterRepository<Labs,Integer> {

   /* @Query(
            "SELECT l" +
                    "FROM Labs" +
                    "WHERE :#{#filter.name} is null or lower(l.name) LIKE :#{#filter.name}"
    ) 
    @Override
    Page<Labs> findAllByFilter(@Param("filter")Filter<Labs> filter, Pageable pageable);*/

    /**
     *Запрос 10:
     * Перечень лабораторя, участвоваших в испытани изделия
     */
    @Query(
            "select distinct l " +
                    "from Labs l right join ProductTesting pt on pt.labEquipment.lab = l " +
                    "where pt.product.id = :productId"
    )
    public Page<Labs> findByProduct(Integer productId, Pageable pageable);
}

