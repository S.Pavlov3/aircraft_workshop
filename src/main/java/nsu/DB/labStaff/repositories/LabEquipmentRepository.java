package nsu.DB.labStaff.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.labStaff.model.LabEquipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface LabEquipmentRepository extends JpaFilterRepository<LabEquipment,Integer> {

   /* @Query(
            "SELECT le" +
                    "FROM LabEquipment" +
                    "WHERE :#{#filter.labId} is null or le.lab.id = :#{#filter.labId}"
    )
    @Override
    Page<LabEquipment> findAllByFilter(@Param("filter")Filter<LabEquipment> filter, Pageable pageable);*/

    /**
     *Запрос 13:
     * Состав оборудования, использованного при испытании определнного изделия, категории изделия
     * и в некторой лаборатории за определенный промежуток времени
     */
    @Query(
            "select le " +
                    "from LabEquipment le left join ProductTesting pt on pt.labEquipment = le " +
                    "where pt.startDate >= :startDate and pt.endDate <= :endDate " +
                    "and le.lab.id = :labId " +
                    "and (pt.product.id = :productId or pt.product.type = :type)"
    )
    public Page<LabEquipment> findByLabAndProductOrProductTypeAndProductTestingDate(
            Date startDate, Date endDate, String type,Integer productId, Integer labId, Pageable pageable);
}

