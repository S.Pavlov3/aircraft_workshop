package nsu.DB.exceptions;

/**
 * Бросается, если какой-либо ресурс не найден
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
