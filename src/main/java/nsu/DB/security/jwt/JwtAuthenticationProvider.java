package nsu.DB.security.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import nsu.DB.security.model.User;
import nsu.DB.security.services.UsersService;


/**
 * Этот класс отвечает за авторизацию пользователя с помощью {@link JwtAuthentication}
 */
@Component
@RequiredArgsConstructor
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final UsersService usersService;

    private final JwtProvider jwtProvider;


    /**
     * Этот метод по сути является авторизацией пользователя по токену.
     * Если все окей этот метод возвращает объект  {@link JwtAuthentication}, null иначе
     * Дальше спринг проверяет: если ему вернули null, то пользователь неавторизирован, иначе
     * он считает пользователя авторизированным и текущим пользователем становится JwtAuthentication.getPrincipal()
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;
        String jwt = jwtAuthentication.getJwt();
        int userId = Integer.parseInt(jwtProvider.getUserId(jwt));
        User user = usersService.getById(userId);

        if (!jwtProvider.validateToken(jwt)) {
            return null;
        }

        jwtAuthentication.setUser(user);
        jwtAuthentication.setAuthenticated(true);
        return jwtAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthentication.class.equals(authentication);
    }
}
