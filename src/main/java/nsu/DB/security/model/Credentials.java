package nsu.DB.security.model;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Credentials {
    //    @Pattern(regexp = EMAIL + "|" + PHONE, message = "Поле 'Логин' некорректно")
    private String login;

    //    @Pattern(regexp = PASSWORD, message = "Поле 'Пароль' некорректно")
    private String password;

}