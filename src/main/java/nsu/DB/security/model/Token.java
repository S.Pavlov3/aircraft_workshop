package nsu.DB.security.model;

import nsu.DB.abstracts.model.Identifiable;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Tokens")
public class Token extends Identifiable {
    public enum Type {
        REFRESH,
        PASSWORD_RESTORE,
        EMAIL_CONFIRM
    }

    String stringRepresentation;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    User user;

    @Temporal(TemporalType.TIMESTAMP)
    Date expirationDate;

    @Enumerated(EnumType.STRING)
    Type type;
}