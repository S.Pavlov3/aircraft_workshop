package nsu.DB.security.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import nsu.DB.security.mappers.UserMapper;
import nsu.DB.security.model.Credentials;
import nsu.DB.security.dto.NewPasswordDto;
import nsu.DB.security.dto.TokenDto;
import nsu.DB.security.dto.UserDto;
import nsu.DB.security.services.AuthorizationService;

import javax.validation.Valid;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
@Slf4j
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    private final UserMapper userMapper;

    @PostMapping("/sign-in")
    public TokenDto signIn(@RequestBody @Valid Credentials credentials) {
        return authorizationService.authenticate(credentials);
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody @Valid UserDto userAuthorizationDto) {
        authorizationService.signUp(userMapper.toEntity(userAuthorizationDto));
    }

    @PostMapping("/restore")
    public void restore(@RequestParam String email) {
        authorizationService.restorePassword(email);
    }

    @PostMapping("/restore/{token}")
    public void restore(@RequestBody NewPasswordDto newPasswordDto,
                        @PathVariable String token) {
        String newPassword = newPasswordDto.getNewPassword();
        authorizationService.changePassword(token, newPassword);
    }

    @GetMapping("/restore/{token}")
    public ModelAndView getRestorePage(@PathVariable String token) {
        authorizationService.validateRestoreToken(token);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/restore.html");

        log.debug("The restore page has been queried.");
        return modelAndView;
    }

    @GetMapping("/confirm")
    public ModelAndView confirmRegistration(@RequestParam String token) {
        authorizationService.confirmEmail(token);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/emailConfirmSuccess.html");

        return modelAndView;
    }
}
