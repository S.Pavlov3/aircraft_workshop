package nsu.DB.security;

import lombok.RequiredArgsConstructor;
import nsu.DB.ApplicationProperties;
import nsu.DB.security.jwt.JwtAuthenticationProvider;
import nsu.DB.security.jwt.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import nsu.DB.security.model.UserRole.Role;

/**
 * Общая конфигурация Spring Security
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthenticationProvider jwtAuthenticationProvider;

    private final JwtRequestFilter jwtRequestFilter;

    private final ApplicationProperties properties;

    private final ExceptionHandlerFilter exceptionHandlerFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
//        http.logout().disable();
        http.httpBasic().disable();
        http.addFilterBefore(jwtRequestFilter, AbstractPreAuthenticatedProcessingFilter.class);
        http.addFilterBefore(exceptionHandlerFilter, JwtRequestFilter.class);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.anonymous()
                .and().authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(properties.getPath() +  "**").hasAuthority(Role.DEFAULT.name());
      //  http.authorizeRequests().antMatchers(properties.getPath()+"/**").permitAll().anyRequest().authenticated();
    }
}
