package nsu.DB.security.events;

import org.springframework.context.ApplicationEvent;
import nsu.DB.security.model.User;


/**
 * Событие генерируется, когда нужно отправить на почту пользователя сообщение с ссылкой для подтвержения почты
 */
public class RegistrationCompleteEvent extends ApplicationEvent {

    public RegistrationCompleteEvent(User user) {
        super(user);
    }

    public User getUser() {
        return (User) super.source;
    }
}
