package nsu.DB.security.events;

import org.springframework.context.ApplicationEvent;
import nsu.DB.security.model.User;


/**
 * Событие создается, когда пользователь хочет восстановить пароль
 */
public class PasswordRestoreEvent extends ApplicationEvent {

    public PasswordRestoreEvent(User user) {
        super(user);
    }

    public User getUser() {
        return (User) super.source;
    }
}
