package nsu.DB.security.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.DB.ApplicationProperties;
import nsu.DB.email.EmailService;
import nsu.DB.security.events.RegistrationCompleteEvent;
import nsu.DB.security.model.Token;
import nsu.DB.security.model.User;
import nsu.DB.security.services.TokenService;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
@Slf4j
public class RegistrationCompleteListener implements ApplicationListener<RegistrationCompleteEvent> {

    private final ApplicationProperties applicationProperties;

    private final EmailService emailService;

    private final TokenService tokenService;

    /**
     * Этот метод вызывается, когда случился {@link RegistrationCompleteEvent}
     * Отправляет на почту пользователя сообщение с ссылкой для подтверждения почты
     * Тут генерируется {@link Token} подтверждения почты
     */
    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        User user = event.getUser();
        String domainName = applicationProperties.getDomainName();
        int port = applicationProperties.getPort();

        try {
            Token token = tokenService.generateToken(user, Token.Type.EMAIL_CONFIRM);
            String confirmUrl = "http://" + domainName + ":" + port + "/confirm?token=" + token.getStringRepresentation();
            String message = "Здравствуйте, " + user.getName()
                    + "!\nНажмите на ссылку ниже, чтобы подтвердить вашу электронную почту:\n" + confirmUrl;
            emailService.sendMessage(user.getEmail(), "Подтверждение почты", message);
        } catch (Exception exc) {
            log.error("Error sending nsu.DB.email confirmation link to user: {}", user.getEmail(), exc);
        }
    }
}
