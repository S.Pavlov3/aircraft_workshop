package nsu.DB.security.listeners;

import nsu.DB.email.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import nsu.DB.security.events.PasswordRestoreEvent;
import nsu.DB.security.model.Token;
import nsu.DB.security.model.User;
import nsu.DB.security.services.TokenService;
import nsu.DB.ApplicationProperties;


@Component
@RequiredArgsConstructor
@Slf4j
public class PasswordRestoreListener implements ApplicationListener<PasswordRestoreEvent> {

    private final ApplicationProperties applicationProperties;

    private final TokenService tokenService;

    private final EmailService emailService;

    /**
     * Этот метод вызывается, когда случился {@link PasswordRestoreEvent}
     * Отправляет на почту пользователя сообщение с ссылкой для восстановления пароля.
     * Тут генерируется {@link Token} восстановления пароля
     */

    @Override
    public void onApplicationEvent(PasswordRestoreEvent event) {
        User user = event.getUser();
        String domainName = applicationProperties.getDomainName();
        int port = applicationProperties.getPort();

        try {
            Token token = tokenService.generateToken(user, Token.Type.PASSWORD_RESTORE);
            String confirmUrl = "http://" + domainName + ":" + port + "/restore/" + token.getStringRepresentation();
            String message = "Hello, " + user.getName()
                    + "!\nclick on clickbait to restore or change your password:\n" + confirmUrl;
            System.out.println(message + user.getEmail());
            emailService.sendMessage(user.getEmail(), "Password change", message);
        } catch (Exception exc) {
            log.error("Error sending password restore link to user: {}", user.getEmail(), exc);
        }
    }
}
