package nsu.DB.security.services;

import nsu.DB.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import nsu.DB.security.model.User;
import nsu.DB.security.repositories.UsersRepository;


@Service
@RequiredArgsConstructor
public class UsersService {

    private final UsersRepository userRepository;

    public User getUserByNickname(String nickname) {
        return userRepository
                .findByNameIgnoreCase(nickname)
                .orElseThrow(() -> new IllegalArgumentException("Wrong nickname"));
    }

    public User getUserByEmail(String email) {
        return userRepository
                .findByEmailIgnoreCase(email)
                .orElseThrow(() -> new IllegalArgumentException("Wrong nsu.DB.email"));
    }

    public void validateUser(User user) {
        if (!getCurrentUser().equals(user)) {
            throw new IllegalArgumentException("Wrong test progress id");
        }
    }

    public User getCurrentUser() {
        return userRepository
                .findCurrentUser()
                .orElseThrow(() -> new IllegalArgumentException("Unauthorized"));
    }

    public User getById(int id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("User not found"));
    }
}
