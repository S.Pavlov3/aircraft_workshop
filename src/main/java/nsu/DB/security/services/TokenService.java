package nsu.DB.security.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import nsu.DB.security.model.Token;
import nsu.DB.security.model.User;
import nsu.DB.security.repositories.TokenRepository;

import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

/**
 * Сервис для манипуляции токенами восстановления пароля и подтверждения почты
 */
@Slf4j
@Service
public class TokenService {
    private final TokenRepository repository;

    private final Clock clock;

    @DurationUnit(ChronoUnit.DAYS)
    private Duration duration;

    public TokenService(TokenRepository tokenRepository, Clock clock, @Value("${token.expiration}") Duration duration) {
        this.repository = tokenRepository;
        this.clock = clock;
        this.duration = duration;
    }

    public User getUser(String stringToken, Token.Type type) {
        Token token = getTokenByStringAndType(stringToken, type);
        return token.getUser();
    }

    public boolean isTokenExpired(Token token) {
        return token.getExpirationDate().before(new Date(clock.millis()));
    }

    public Token validateToken(String stringToken, Token.Type type) {
        Token token = getTokenByStringAndType(stringToken, type);
        if (isTokenExpired(token)) {
            throw new IllegalArgumentException("Wrong token");
        }

        return token;
    }

    public void removeToken(int id) {
        repository.deleteById(id);
    }

    @Transactional
    public Token generateToken(User user, Token.Type type) {
        Token token = new Token();
        Date expirationDate = new Date(clock.millis() + duration.toMillis());
        String stringToken = UUID.randomUUID().toString();
        token.setUser(user);
        token.setExpirationDate(expirationDate);
        token.setStringRepresentation(stringToken);
        token.setType(type);

        log.info("Created token for use {}. Type of token : {}", user.getName(), type);
        return repository.save(token);
    }

    protected Token getTokenByStringAndType(String stringToken, Token.Type type) {
        return repository
                .findByStringRepresentationAndType(stringToken, type)
                .orElseThrow(() -> new IllegalArgumentException("Wrong token"));
    }
}
