package nsu.DB.security.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import nsu.DB.security.AuthorizationTokenProvider;
import nsu.DB.security.dto.TokenDto;
import nsu.DB.security.events.PasswordRestoreEvent;
import nsu.DB.security.events.RegistrationCompleteEvent;
import nsu.DB.security.model.Credentials;
import nsu.DB.security.model.Token;
import nsu.DB.security.model.User;
import nsu.DB.security.model.UserRole;
import nsu.DB.security.repositories.UserRolesRepository;
import nsu.DB.security.repositories.UsersRepository;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorizationService {
    private final UsersRepository userRepository;

    private final UsersService usersService;

    private final UserRolesRepository roleRepository;

    private final PasswordEncoder bCryptPasswordEncoder;

    private final AuthorizationTokenProvider jwtProvider;

    private final ApplicationEventPublisher eventPublisher;


    private final TokenService tokenService;

    public void signUp(User user) {
        checkUniqueParams(user);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.getRoles().add(roleRepository.findByRole(UserRole.Role.UNCONFIRMED));

        log.info("User {} signed up", user.getName());
        eventPublisher.publishEvent(new RegistrationCompleteEvent(userRepository.save(user)));
    }

    @Transactional
    public TokenDto authenticate(Credentials credentials) {
        User user = usersService.getUserByNickname(credentials.getLogin());

        if (!isConfirmed(user)) {
            throw new IllegalArgumentException("Unconfirmed");
        }

        if (!bCryptPasswordEncoder.matches(credentials.getPassword(), user.getPassword())) {
            throw new IllegalArgumentException("Wrong credentials");
        }

        log.info("User {} logged in", user.getName());
        String jwt = jwtProvider.generateToken(user.getId());
        return new TokenDto(jwt);
    }

    public void restorePassword(String email) {
        User user = usersService.getUserByEmail(email);

        log.info("User {} sent request to restore password", email);
        eventPublisher.publishEvent(new PasswordRestoreEvent(user));
    }

    public void validateRestoreToken(String token) {
        tokenService.validateToken(token, Token.Type.PASSWORD_RESTORE);

        log.info("Restore token {} was validated", token);
    }


    @Transactional
    public void changePassword(String stringToken, String newPassword) {
        Token token = tokenService.validateToken(stringToken, Token.Type.PASSWORD_RESTORE);
        User user = tokenService.getUser(stringToken, Token.Type.PASSWORD_RESTORE);

        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        tokenService.removeToken(token.getId());

        log.info("User {} changed password", user.getName());
    }

    @Transactional
    public void confirmEmail(String stringToken) {
        Token token = tokenService.validateToken(stringToken, Token.Type.EMAIL_CONFIRM);
        User user = tokenService.getUser(stringToken, Token.Type.EMAIL_CONFIRM);
        if (isConfirmed(user)) {
            throw new IllegalArgumentException("User already confirmed");
        }

        UserRole unconfirmedRole = roleRepository.findByRole(UserRole.Role.UNCONFIRMED);
        UserRole defaultRole = roleRepository.findByRole(UserRole.Role.DEFAULT);
        user.getRoles().remove(unconfirmedRole);
        user.getRoles().add(defaultRole);

        tokenService.removeToken(token.getId());

        log.info("User {} confirmed his nsu.DB.email {}", user.getName(), user.getEmail());
    }

    private boolean isConfirmed(User user) {
        return user.getRoles()
                .stream()
                .map(UserRole::getAuthority)
                .noneMatch(r -> r.equals(UserRole.Role.UNCONFIRMED.name()));
    }


    private void checkUniqueParams(User user) {
        if (userRepository.findByEmailIgnoreCase(user.getEmail()).isPresent()) {
            throw new IllegalArgumentException("User with such nsu.DB.email already exists");
        }

        if (userRepository.findByNameIgnoreCase(user.getName()).isPresent()) {
            throw new IllegalArgumentException("User with such nickname already exists");
        }
    }

}
