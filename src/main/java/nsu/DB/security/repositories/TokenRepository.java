package nsu.DB.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import nsu.DB.security.model.Token;
import nsu.DB.security.model.User;


import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token, Integer> {
    void deleteByUserAndType(User user, Token.Type type);

    Optional<Token> findByStringRepresentationAndType(String stringRepresentation, Token.Type type);
}
