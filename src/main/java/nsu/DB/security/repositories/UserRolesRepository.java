package nsu.DB.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import nsu.DB.security.model.UserRole;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByRole(UserRole.Role role);
}
