package nsu.DB.security.dto;

import lombok.Data;

@Data
public class NewPasswordDto {
    //    @Pattern(regexp = RegexTemplateConstants.PASSWORD, message = "Некорректное поле 'Новый пароль'")
    private String newPassword;
}
