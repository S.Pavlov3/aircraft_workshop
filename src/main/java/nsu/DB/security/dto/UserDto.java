package nsu.DB.security.dto;

import lombok.Data;

@Data
public class UserDto {
    String name;

    String email;

    String fullName;

    String password;
}