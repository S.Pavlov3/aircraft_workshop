package nsu.DB.security.mappers;

import nsu.DB.abstracts.mappers.Mapper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import nsu.DB.security.dto.UserDto;
import nsu.DB.security.model.User;


@Component
@RequiredArgsConstructor
public class UserMapper extends Mapper<User, UserDto> {
    private final ModelMapper modelMapper;

    @Override
    public User toEntity(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDto toDto(User entity) {
        return modelMapper.map(entity, UserDto.class);
    }
}
