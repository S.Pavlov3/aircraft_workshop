package nsu.DB.email;

import org.springframework.stereotype.Service;

/**
 * Интерфейс для работы с почтовым клиентом
 */
@Service
public interface EmailService {
    void sendMessage(String[] to, String subject, String body);

    void sendMessage(String to, String subject, String body);
}
