package nsu.DB.companyStaff.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.dto.AreasDto;
import nsu.DB.companyStaff.model.Areas;
import nsu.DB.companyStaff.services.WorkshopsService;
import nsu.DB.employees.services.EngineersService;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AreasMapper extends Mapper<Areas, AreasDto> {

    private final WorkshopsService workshopsService;

    private final EngineersService areaSuperiorsService;

    private final ModelMapper modelMapper;

    @Override
    public Areas toEntity(AreasDto dto) throws NoDataFoundException {
        Areas entity = modelMapper.map(dto, Areas.class);
        entity.setSerialNumber(dto.getSerialNumber());
        Integer workshopId = dto.getWorkshopId();
        if(workshopId != null) {
            entity.setWorkshop(workshopsService.getEntity(workshopId));
        }
        else{
            throw new NoDataFoundException("Wrong workshopId");
        }
       // if(dto.getAreaSuperiorId()!=null)
            entity.setAreaSuperior(areaSuperiorsService.getEntity(dto.getAreaSuperiorId()));
       // else
       //     throw new NoDataFoundException("Wrong areaSuperiorId");
        return entity;
    }

    @Override
    public AreasDto toDto(Areas entity) {
        AreasDto dto = modelMapper.map(entity,AreasDto.class);
        dto.setWorkshopId(entity.getWorkshop()!=null ? entity.getWorkshop().getId() : null);
        dto.setSerialNumber(entity.getSerialNumber());
        dto.setAreaSuperiorId(entity.getAreaSuperior()!=null ? entity.getAreaSuperior().getId() : null);
        return dto;
    }
}
