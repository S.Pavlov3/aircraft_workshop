package nsu.DB.companyStaff.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.dto.WorkshopsDto;
import nsu.DB.companyStaff.model.Workshops;
import nsu.DB.employees.services.EngineersService;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WorkshopsMapper extends Mapper<Workshops, WorkshopsDto> {

    private final ModelMapper modelMapper;

    private final EngineersService workshopSuperiorsService;

    @Override
    public Workshops toEntity(WorkshopsDto dto) throws NoDataFoundException {
        Workshops entity = modelMapper.map(dto, Workshops.class);
        entity.setName(dto.getName());
       // if(dto.getWorkshopSuperiorId()!=null)
            entity.setWorkshopSuperior(workshopSuperiorsService.getEntity(dto.getWorkshopSuperiorId()));
        //else throw new NoDataFoundException("Wrong workshopSuperiorId");
        return entity;
    }

    @Override
    public WorkshopsDto toDto(Workshops entity) {
        WorkshopsDto dto = modelMapper.map(entity, WorkshopsDto.class);
        dto.setName(entity.getName());
        dto.setWorkshopSuperiorId(entity.getWorkshopSuperior()!=null ?
                entity.getWorkshopSuperior().getId() : null);
        return dto;
    }
}
