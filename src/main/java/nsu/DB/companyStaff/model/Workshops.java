package nsu.DB.companyStaff.model;

import nsu.DB.abstracts.model.Identifiable;
import lombok.Getter;
import lombok.Setter;
import nsu.DB.employees.model.Engineers;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "Workshops")
public class Workshops extends Identifiable {
    
    private String Name;

    @OneToOne
    @JoinColumn(name = "workshopSuperiorId", referencedColumnName = "id")
    private Engineers workshopSuperior;
}
