package nsu.DB.companyStaff.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.employees.model.Engineers;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "Areas")
public class Areas extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "workshopId",referencedColumnName = "id")
    private Workshops workshop;

    private int serialNumber;

    @OneToOne
    @JoinColumn(name = "areaSuperiorId", referencedColumnName = "id")
    private Engineers areaSuperior;
}
