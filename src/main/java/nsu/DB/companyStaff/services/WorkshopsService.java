package nsu.DB.companyStaff.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.companyStaff.model.Workshops;
import nsu.DB.companyStaff.repositories.WorkshopsRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkshopsService extends AbstractCrudService<Workshops> {
    public WorkshopsService(WorkshopsRepository repository){ this.setRepository(repository);}
}
