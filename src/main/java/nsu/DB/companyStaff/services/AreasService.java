package nsu.DB.companyStaff.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.companyStaff.model.Areas;
import nsu.DB.companyStaff.repositories.AreasRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AreasService extends AbstractCrudService<Areas> {
    public AreasService(AreasRepository repository){ this.setRepository(repository);}

    public Page<Areas> findByWorkshop(Integer workshopId, FindRequestParameters parameters){
        Pageable pageable =getPageable(parameters);
        return ((AreasRepository)getRepository()).findByWorkshop(workshopId,pageable);
    }
}
