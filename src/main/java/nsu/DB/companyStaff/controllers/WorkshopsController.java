package nsu.DB.companyStaff.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.companyStaff.dto.WorkshopsDto;
import nsu.DB.companyStaff.mappers.WorkshopsMapper;
import nsu.DB.companyStaff.model.Workshops;
import nsu.DB.companyStaff.services.WorkshopsService;
import nsu.DB.utils.AppPath;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/workshops")
public class WorkshopsController extends AbstractCrudController<Workshops, WorkshopsDto> {

    public WorkshopsController(WorkshopsService service, WorkshopsMapper mapper) {
        super(service,mapper,"Workshop");
    }

/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(WorkshopsFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
}
