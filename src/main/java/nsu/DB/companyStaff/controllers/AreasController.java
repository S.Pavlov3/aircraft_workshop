package nsu.DB.companyStaff.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.companyStaff.dto.AreasDto;
import nsu.DB.companyStaff.mappers.AreasMapper;
import nsu.DB.companyStaff.model.Areas;
import nsu.DB.companyStaff.services.AreasService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/areas")
public class AreasController extends AbstractCrudController<Areas, AreasDto> {

    public AreasController(AreasService service, AreasMapper mapper) {
        super(service, mapper, "Areas");
    }

/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(AreasFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     * Запрос 4:
     * Перечень участков указанного цеха, предприятия их их начальников
     **/
    @GetMapping("/by-workshop")
    public PageDto<?> getByWorkshop (@RequestParam Integer workshopId, FindRequestParameters parameters){
        Page<Areas> page = ((AreasService)getService()).findByWorkshop(workshopId,parameters);
        return getMapper().toPageDto(page);
    }
}
