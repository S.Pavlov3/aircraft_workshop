package nsu.DB.companyStaff.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.companyStaff.model.Workshops;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkshopsRepository extends JpaFilterRepository<Workshops, Integer> {

    /*@Query(
            "select w " +
                    "from workshops" +
                    "where :#{#filter.name} is null or lower(w.name) like :#{#filter.name}"
    )
    @Override
    Page<Workshops> findAllByFilter(@Param("filter")Filter<Workshops> filter, Pageable pageable);*/
}

