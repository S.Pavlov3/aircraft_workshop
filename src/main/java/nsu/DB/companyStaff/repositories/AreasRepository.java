package nsu.DB.companyStaff.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.companyStaff.model.Areas;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AreasRepository extends JpaFilterRepository<Areas, Integer> {


    /*@Query(
        "select areas " +
                "from Areas " +
                "where :#{#filter.workshopId} is null or areas.workshop.id = :#{#filter.workshopId}" +
                "and :#{#filter.serialNumber} is null or areas.serialNumber = :#{#filter.serialNumber}"
    )
    @Override
    Page<Areas> findAllByFilter(@Param("filter")Filter<Areas> filter, Pageable pageable);*/

    /**
     * Запрос 4:
     * Перечень участков указанного цеха, предприятия их их начальников
    **/
    @Query(
            "select a " +
                    "from Areas a " +
                    "where :workshopId is null or a.workshop.id = :workshopId"
    )
    public Page<Areas> findByWorkshop(Integer workshopId, Pageable pageable);
}

