package nsu.DB.companyStaff.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.companyStaff.model.Areas;

@Getter
@Setter
@NoArgsConstructor
public class AreasDto extends Dto<Areas> {
    private Integer workshopId;
    private Integer serialNumber;
    private Integer areaSuperiorId;
}
