package nsu.DB.companyStaff.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.companyStaff.model.Workshops;

@Getter
@Setter
@NoArgsConstructor
public class WorkshopsDto extends Dto<Workshops> {
    
    private String name;
    private Integer workshopSuperiorId;
}
