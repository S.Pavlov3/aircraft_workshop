package nsu.DB.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import nsu.DB.abstracts.model.Identifiable;

public class UtilFunctions {

    public static Pageable getPageable(FindRequestParameters parameters, Sort sort) {
        if(parameters!=null){
            return PageRequest.of(parameters.getPage(),parameters.getPageSize(),sort);
        }
            return Pageable.unpaged();
    }

    public static<E> List<E> getNullableList(List<E> list){
        if(list.size()!=0)return list;
        return null;
    }
    
    public static Sort getSort(FindRequestParameters parameters){
        if(parameters != null){
            var order = parameters.getOrder();
            var orderBy = parameters.getOrderBy();
            if(order != null && orderBy != null){
                if(order == FindRequestParameters.Order.DESCENDING){
                    Sort.by(orderBy).descending();
                }
                else{
                    Sort.by(orderBy).ascending();
                }
            }
        }
        return Sort.unsorted();
    }
    
    public static <E extends Identifiable> Set<Integer> identifiablesToIds(Set<E> entities){
        return entities.stream().map(Identifiable::getId).collect(Collectors.toSet());
        
    }

}
