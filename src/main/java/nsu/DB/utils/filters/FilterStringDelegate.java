package nsu.DB.utils.filters;

public class FilterStringDelegate {

    public String initial;

    private String backingString = formatString(initial);

    private String formatString(String value){
        if(value.equals("") || value == null){
            return null;
        }
        return value.toLowerCase();
    }

    /*private var backingString: String? = formatString(initial)

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String? {
        return backingString
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) {
        backingString = formatString(value)
    }

    private fun formatString(value: String?): String? {
        if (value == "" || value == null) {
            return null
        }
        return "%${value.toLowerCase()}%"
    }*/
    
}
