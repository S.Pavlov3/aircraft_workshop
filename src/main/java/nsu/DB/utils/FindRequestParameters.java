package nsu.DB.utils;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FindRequestParameters {
    public enum Order {
        ASCENDING,
        DESCENDING
    }
    
    private int page;
    private int pageSize;
    private String orderBy;
    private Order order;
}
