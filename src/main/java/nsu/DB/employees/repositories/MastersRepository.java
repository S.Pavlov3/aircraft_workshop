package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.Masters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MastersRepository extends JpaFilterRepository<Masters, Integer> {

/*    @Query(
            ""
    )
    @Override
    Page<Masters> findAllByFilter(@Param("filter")Filter<Masters> filter, Pageable pageable);*/

    /**
     * Запрос 7:
     * Список мастеров указанного цеха, участка
     */
    @Query(
            "select m " +
                    "from Masters m left join Areas a on m.area = a " +
                    "where a.id = :areasId " +
                    "or a.workshop.id = :workshopsId"
    )
    public Page<Masters> findByAreaOrWorkshop(Integer areasId, Integer workshopsId, Pageable pageable);
}

