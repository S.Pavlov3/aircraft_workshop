package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.WorkshopSuperiors;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkshopSuperiorsRepository extends JpaFilterRepository<WorkshopSuperiors,Integer> {

    /*@Query(
            ""
    )
    @Override
    Page<WorkshopSuperiors> findAllByFilter(@Param("filter") Filter<WorkshopSuperiors> filter, Pageable pageable);
*/}

