package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.Brigades;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BrigadesRepository extends JpaFilterRepository<Brigades, Integer> {

   /*@Query(
            "select b" +
                    "from Brigades" +
                    "where :#{#filter.areaId} is null or b.area.id  :#{#filter.areaId}" +
                    "and :#{#filter.brigadeLeaderId} is null or as.area.id  :#{#filter.brigadeLeaderId}"
    )
    @Override
    Page<Brigades> findAllByFilter(@Param("filter")Filter<Brigades> filter, Pageable pageable);*/

    /**
     * Запрос 9:
     *Состав бригад, участвовавших в сборке изделия
     */
    @Query(
        "select b " +
                "from Brigades b left join Assembly a on a.brigade = b " +
                "where a.product.id = :productId"
    )
    public Page<Brigades> findByAssembly(Integer productId, Pageable pageable);

    /**
     *Запрос 6:
     * Состав бригад указанного цеха, участка
    */
    @Query(
            "select b " +
                    "from Brigades b left join Areas a on b.area = a " +
                    "where a.id = :areasId " +
                    "or a.workshop.id = :workshopsId"
    )
    public Page<Brigades> findByAreaOrWorkshop(Integer areasId, Integer workshopsId, Pageable pageable);
}

