package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.Engineers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EngineersRepository extends JpaFilterRepository<Engineers, Integer> {

/*    @Query(
            ""
    )
    @Override
    Page<Engineers> findAllByFilter(@Param("filter")Filter<Engineers> filter, Pageable pageable);*/

    @Query(
            "select e " +
                    "from Engineers e left join Masters m on e.master = m " +
                    "where :workshopId is null or m.area.workshop.id = :workshopId"
    )
    public Page<Engineers> findByWorkshop(Integer workshopId, Pageable pageable);
}

