package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.AreaSuperiors;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaSuperiorsRepository extends JpaFilterRepository<AreaSuperiors, Integer> {
    /*@Query(
            "select as" +
                    "from AreaSuperiors" +
                    "where :#{#filter.areaId} is null or as.area.id  :#{#filter.areaId}" +
                    "and :#{#filter.name} is null or as.name = :#{#filter.name}" +
                    "and :#{#filter.surname} is null or as.surname = :#{#filter.surname}" +
                    "and :#{#filter.middleName} is null or as.middleName = :#{#filter.middleName}" +
                    "and :#{#filter.profession} is null or as.profession = :#{#filter.profession}"
    )
    @Override
    Page<AreaSuperiors> findAllByFilter(@Param("filter")Filter<AreaSuperiors> filter, Pageable pageable);*/
}

