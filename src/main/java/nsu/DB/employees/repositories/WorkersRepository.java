package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.Workers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkersRepository extends JpaFilterRepository<Workers, Integer> {

/*    @Query(
            ""
    )
    @Override
    Page<Workers> findAllByFilter(@Param("filter")Filter<Workers> filter, Pageable pageable);*/
    @Query(
        "select w " +
                "from Workers w right join Brigades b on w.brigade = b " +
                "where :workshopId is null or b.area.workshop.id = :workshopId"
    )
    public Page<Workers> findByWorkshop (Integer workshopId,Pageable pageable);

}

