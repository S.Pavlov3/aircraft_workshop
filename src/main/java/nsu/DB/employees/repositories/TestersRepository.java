package nsu.DB.employees.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.employees.model.Testers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TestersRepository extends JpaFilterRepository<Testers, Integer> {

/*    @Query(
            ""
    )
    @Override
    Page<Testers> findAllByFilter(@Param("filter")Filter<Testers> filter, Pageable pageable);*/

    /**
     * Запрос 12:
     * Список испытателей, участвоваших в сбореке изделия, изделий и в некоторой лаборатории за
     * определенный промежуток времени
    */
     @Query(
            "select t " +
                    "from Testers t left join ProductTesting pt on pt.tester = t " +
                    "where pt.startDate >= :startDate and pt.endDate <= :endDate " +
                    "and pt.product.id in :productIds " +
                    "and pt.labEquipment.lab.id = :labId"
    )
    public Page<Testers> findByLabAndProductAndProductTestingDate(
             Date startDate, Date endDate, List<Integer> productIds, Integer labId, Pageable pageable);
}

