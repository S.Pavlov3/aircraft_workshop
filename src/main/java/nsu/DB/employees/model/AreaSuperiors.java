package nsu.DB.employees.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.companyStaff.model.Areas;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "AreaSuperiors")
public class AreaSuperiors extends Engineers{

    @OneToOne
    @JoinColumn(name = "areaId",referencedColumnName = "id")
    private Areas area;
}
