package nsu.DB.employees.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Persons;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Setter
@Getter
@Entity
@Table(name = "Workers")
public class Workers extends Persons {
    
    private String profession;

    @ManyToOne
    @JoinColumn(name= "brigadeId", referencedColumnName = "id")
    private Brigades brigade;
}
