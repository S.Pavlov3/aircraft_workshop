package nsu.DB.employees.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.companyStaff.model.Areas;

import javax.persistence.*;
import java.util.Set;

@Entity
@Setter
@Getter
@Table(name = "Brigades")
public class Brigades extends Identifiable {

    @OneToOne
    @JoinColumn(name = "areaId", referencedColumnName = "id")
    private Areas area;

    @OneToOne
    @JoinColumn(name = "brigadeLeaderId",referencedColumnName = "id")
    private Workers brigadeLeader;

    @OneToMany
    private Set<Workers> workers;
}
