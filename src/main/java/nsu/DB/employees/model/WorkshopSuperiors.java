package nsu.DB.employees.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.companyStaff.model.Workshops;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "WorkshopSuperiors")
public class WorkshopSuperiors extends Engineers {

    @OneToOne
    @JoinColumn(name = "workshopId",referencedColumnName = "id")
    private Workshops workshop;
}
