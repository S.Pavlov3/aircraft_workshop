package nsu.DB.employees.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.companyStaff.model.Areas;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "Masters")
public class Masters extends Engineers{

    @ManyToOne
    @JoinColumn(name = "areaId",referencedColumnName = "id")
    private Areas area;

}
