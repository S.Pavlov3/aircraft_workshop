package nsu.DB.employees.filters;

import nsu.DB.employees.model.Workers;
import nsu.DB.utils.filters.Filter;

public class WorkersFilter implements Filter<Workers> {
    public String name;
    public String surname;
    public String middleName;
    public String profession;
}
