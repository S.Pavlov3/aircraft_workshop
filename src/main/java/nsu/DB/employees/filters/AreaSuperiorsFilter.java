package nsu.DB.employees.filters;

import nsu.DB.employees.model.AreaSuperiors;
import nsu.DB.utils.filters.Filter;

public class AreaSuperiorsFilter implements Filter<AreaSuperiors> {
    public String name;
    public String surname;
    public String middleName;
    public String profession;
    public int areaId;
}
