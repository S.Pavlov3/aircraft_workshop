package nsu.DB.employees.filters;

import nsu.DB.employees.model.Testers;
import nsu.DB.utils.filters.Filter;

public class TestersFilter implements Filter<Testers> {
    public String name;
    public String surname;
    public String middleName;
}
