package nsu.DB.employees.filters;

import nsu.DB.employees.model.WorkshopSuperiors;
import nsu.DB.utils.filters.Filter;

public class WorkshopSuperiorsFilter implements Filter<WorkshopSuperiors> {
    public String name;
    public String surname;
    public String middleName;
    public String profession;
    public int workshopId;
}
