package nsu.DB.employees.filters;

import nsu.DB.employees.model.Engineers;
import nsu.DB.utils.filters.Filter;

public class EngineersFilter implements Filter<Engineers> {
    public String name;
    public String surname;
    public String middleName;
    public String profession;
}
