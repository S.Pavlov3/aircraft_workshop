package nsu.DB.employees.filters;

import nsu.DB.employees.model.Masters;
import nsu.DB.utils.filters.Filter;

public class MastersFilter implements Filter<Masters> {
    public String name;
    public String surname;
    public String middleName;
    public String profession;
    public int areaSuperiorId;
}
