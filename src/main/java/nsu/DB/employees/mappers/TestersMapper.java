package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.employees.dto.TestersDto;
import nsu.DB.employees.model.Testers;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TestersMapper extends Mapper<Testers, TestersDto> {
    private final ModelMapper modelMapper;

    @Override
    public Testers toEntity(TestersDto dto) {
        Testers entity = modelMapper.map(dto,Testers.class);
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public TestersDto toDto(Testers entity) {
        TestersDto dto = modelMapper.map(entity,TestersDto.class);
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
