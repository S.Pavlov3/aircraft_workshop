package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.services.AreasService;
import nsu.DB.employees.dto.MastersDto;
import nsu.DB.employees.model.Masters;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MastersMapper extends Mapper<Masters, MastersDto> {
    private final ModelMapper modelMapper;

    private final AreasService areasService;

    @Override
    public Masters toEntity(MastersDto dto) throws NoDataFoundException {
        Masters entity = modelMapper.map(dto,Masters.class);
        Integer areaSuperiorId = dto.getAreaId();
        if(areaSuperiorId != null) {
            entity.setArea(areasService.getEntity(areaSuperiorId));
        }
        else{
            throw new NoDataFoundException("Wrong areaSuperiorId");
        }
        if(dto.getProfession()!=null) {
            entity.setProfession(dto.getProfession());
        }
        else{
            throw new NoDataFoundException("No profession profession");
        }
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public MastersDto toDto(Masters entity) {
        MastersDto dto = modelMapper.map(entity,MastersDto.class);
        dto.setAreaId(entity.getArea()!= null ? entity.getArea().getId() : null);
        dto.setProfession(entity.getProfession());
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
