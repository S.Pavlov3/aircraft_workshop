package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.services.AreasService;
import nsu.DB.employees.dto.BrigadesDto;
import nsu.DB.employees.dto.WorkersDto;
import nsu.DB.employees.model.Brigades;
import nsu.DB.employees.model.Workers;
import nsu.DB.employees.services.WorkersService;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BrigadesMapper extends Mapper<Brigades, BrigadesDto> {
    private final ModelMapper modelMapper;

    private final AreasService areasService;

    private final WorkersService workersService;

    @Override
    public Brigades toEntity(BrigadesDto dto) throws NoDataFoundException {
        Brigades entity = modelMapper.map(dto, Brigades.class);
        Integer areaId = dto.getAreaId();
        Integer brigadeLeaderId = dto.getBrigadeLeaderId();
        if(areaId != null) {
            entity.setArea(areasService.getEntity(areaId));
        }
        else{
            throw new NoDataFoundException("Wrong areaId");
        }
        if(brigadeLeaderId!=null) {
            entity.setBrigadeLeader(workersService.getEntity(brigadeLeaderId));
        }
        else{
            throw new NoDataFoundException("Wrong brigadeLeaderId");
        }
        Set<Workers> workers = dto.getWorkersIds().stream().
                map((s)-> {
                    try {
                        return workersService.getEntity(s);
                    } catch (NoDataFoundException e) {
                        e.printStackTrace();
                    }
                return null;
                }).collect(Collectors.toSet());
        entity.setWorkers(workers);
        return entity;
    }

    @Override
    public BrigadesDto toDto(Brigades entity) {
        BrigadesDto dto = modelMapper.map(entity, BrigadesDto.class);
        dto.setAreaId(entity.getArea()!= null ? entity.getArea().getId() : null);
        dto.setBrigadeLeaderId(entity.getBrigadeLeader()!=null? entity.getBrigadeLeader().getId(): null);
        Set<Integer> workersIds = entity.getWorkers().stream().
                map((s)->modelMapper.map(s, WorkersDto.class).getId()).collect(Collectors.toSet());
        dto.setWorkersIds(workersIds);
        return dto;
    }
}
