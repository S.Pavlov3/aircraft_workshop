package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.employees.dto.EngineersDto;
import nsu.DB.employees.model.Engineers;
import nsu.DB.employees.services.MastersService;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EngineersMapper extends Mapper<Engineers, EngineersDto> {
    private final ModelMapper modelMapper;

    private final MastersService mastersService;

    @Override
    public Engineers toEntity(EngineersDto dto) throws NoDataFoundException {
        Engineers entity = modelMapper.map(dto, Engineers.class);
        if(dto.getProfession() == null) {
            throw new NoDataFoundException("No profession profession");
        }
        entity.setProfession(dto.getProfession());
        if(dto.getMastersId()!=null)
            entity.setMaster(mastersService.getEntity(dto.getMastersId()));
        else entity.setMaster(null);
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public EngineersDto toDto(Engineers entity) {
        EngineersDto dto = modelMapper.map(entity,EngineersDto.class);
        dto.setProfession(entity.getProfession());
        dto.setMastersId(entity.getMaster()!=null ? entity.getMaster().getId() : null);
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
