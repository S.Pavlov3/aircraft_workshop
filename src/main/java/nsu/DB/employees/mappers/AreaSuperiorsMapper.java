package nsu.DB.employees.mappers;

import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.services.AreasService;
import nsu.DB.employees.dto.AreaSuperiorDto;
import nsu.DB.employees.model.AreaSuperiors;
import lombok.RequiredArgsConstructor;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AreaSuperiorsMapper extends Mapper<AreaSuperiors, AreaSuperiorDto> {

    private final ModelMapper modelMapper;

    private final AreasService areasService;

    @Override
    public AreaSuperiors toEntity(AreaSuperiorDto dto) throws NoDataFoundException {
        AreaSuperiors entity = modelMapper.map(dto,AreaSuperiors.class);
        Integer areaId = dto.getAreaId();
        if(areaId != null) {
            entity.setArea(areasService.getEntity(areaId));
        }
        else{
            throw new NoDataFoundException("No profession profession");
        }
        entity.setProfession(dto.getProfession());
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public AreaSuperiorDto toDto(AreaSuperiors entity) {
        AreaSuperiorDto dto = modelMapper.map(entity, AreaSuperiorDto.class);
        dto.setAreaId(entity.getArea()!=null ? entity.getArea().getId() : null);
        dto.setProfession(entity.getProfession());
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
