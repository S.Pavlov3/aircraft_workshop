package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.services.WorkshopsService;
import nsu.DB.employees.dto.WorkshopSuperiorsDto;
import nsu.DB.employees.model.WorkshopSuperiors;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WorkshopSuperiorsMapper extends Mapper<WorkshopSuperiors, WorkshopSuperiorsDto> {
    private final ModelMapper modelMapper;

    private final WorkshopsService workshopsService;

    @Override
    public WorkshopSuperiors toEntity(WorkshopSuperiorsDto dto) throws NoDataFoundException {
        WorkshopSuperiors entity = modelMapper.map(dto,WorkshopSuperiors.class);
        Integer workshopId = dto.getWorkshopId();
        if(workshopId!= null) {
            entity.setWorkshop(workshopsService.getEntity(workshopId));
        }
        else{
            throw new NoDataFoundException("Wrong workshopId");
        }
        if(dto.getProfession()!=null) {
            entity.setProfession(dto.getProfession());
        }
        else{
            throw new NoDataFoundException("No profession profession");
        }
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public WorkshopSuperiorsDto toDto(WorkshopSuperiors entity) {
        WorkshopSuperiorsDto dto = modelMapper.map(entity, WorkshopSuperiorsDto.class);
        dto.setProfession(entity.getProfession());
        dto.setWorkshopId(entity.getWorkshop()!=null ? entity.getWorkshop().getId() : null);
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
