package nsu.DB.employees.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.employees.dto.WorkersDto;
import nsu.DB.employees.model.Workers;
import nsu.DB.employees.services.BrigadesService;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WorkersMapper extends Mapper<Workers, WorkersDto> {
    private final ModelMapper modelMapper;

    private final BrigadesService brigadesService;

    @Override
    public Workers toEntity(WorkersDto dto) throws NoDataFoundException {
        Workers entity = modelMapper.map(dto,Workers.class);
        if(dto.getProfession()!=null) {
            entity.setProfession(dto.getProfession());
        }
        else{
            throw new NoDataFoundException("No profession");
        }
        if(dto.getBrigadeId()!=null) {
            entity.setBrigade(brigadesService.getEntity(dto.getBrigadeId()));
        }
        else{
            entity.setBrigade(null);
        }
        entity.setName(dto.getName());
        entity.setMiddleName(dto.getMiddleName());
        entity.setSurname(dto.getSurname());
        return entity;
    }

    @Override
    public WorkersDto toDto(Workers entity) {
        WorkersDto dto = modelMapper.map(entity,WorkersDto.class);
        dto.setProfession(entity.getProfession());
        dto.setBrigadeId(entity.getBrigade()!=null ? entity.getBrigade().getId() : null);
        dto.setName(entity.getName());
        dto.setMiddleName(entity.getMiddleName());
        dto.setSurname(entity.getSurname());
        return dto;
    }
}
