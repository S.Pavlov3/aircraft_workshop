package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.MastersDto;
import nsu.DB.employees.mappers.MastersMapper;
import nsu.DB.employees.model.Masters;
import nsu.DB.employees.services.MastersService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/masters")
public class MastersController extends AbstractCrudController<Masters, MastersDto> {

    public MastersController(MastersService service, MastersMapper mapper) {
        super(service, mapper, "Masters");
    }

/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(MastersFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     * Запрос 7:
     * Список мастеров указанного цеха, участка
     */
    @GetMapping("by-area-or-workshop")
    public PageDto<?> getByAreaOrWorkshop(
            Integer areasId, Integer workshopsId, FindRequestParameters parameters){
        Page<Masters> page = ((MastersService)getService()).findByAreaOrWorkshop(areasId, workshopsId, parameters);
        return getMapper().toPageDto(page);
    }
}
