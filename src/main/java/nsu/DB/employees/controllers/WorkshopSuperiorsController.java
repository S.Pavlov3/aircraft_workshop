package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.employees.dto.WorkshopSuperiorsDto;
import nsu.DB.employees.mappers.WorkshopSuperiorsMapper;
import nsu.DB.employees.model.WorkshopSuperiors;
import nsu.DB.employees.services.WorkshopSuperiorsService;
import nsu.DB.utils.AppPath;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/workshopSuperiors")
public class WorkshopSuperiorsController extends
        AbstractCrudController<WorkshopSuperiors, WorkshopSuperiorsDto> {

    public WorkshopSuperiorsController(WorkshopSuperiorsService service, WorkshopSuperiorsMapper mapper) {
        super(service, mapper, "WorkshopSuperiors");
    }


 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(WorkshopSuperiorsFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
}
