package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.BrigadesDto;
import nsu.DB.employees.mappers.BrigadesMapper;
import nsu.DB.employees.model.Brigades;
import nsu.DB.employees.services.BrigadesService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+ "/brigades")
public class BrigadesController extends AbstractCrudController<Brigades, BrigadesDto> {

    public BrigadesController(BrigadesService service, BrigadesMapper mapper) {
        super(service, mapper, "Brigades");
    }

 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(BrigadesFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     * Запрос 9:
     *Состав бригад, участвовавших в сборке изделия
     */
    @GetMapping("by-assembly")
    public PageDto<?> getByAssembly(@RequestParam Integer productId, FindRequestParameters requestParameters){
        Page<Brigades> page = ((BrigadesService)getService()).findByAssembly(productId, requestParameters);
        return getMapper().toPageDto(page);
    }
    /**
     *Запрос 6:
     * Состав бригад указанного цеха, участка
     */
    @GetMapping("by-area-or-workshop")
    public PageDto<?> getByAreaOrWorkshop(
            @RequestParam Integer areasId,@RequestParam Integer workshopsId,FindRequestParameters parameters){
        Page<Brigades> page = ((BrigadesService)getService()).findByAreaOrWorkshop(areasId, workshopsId, parameters);
        return getMapper().toPageDto(page);
    }
}
