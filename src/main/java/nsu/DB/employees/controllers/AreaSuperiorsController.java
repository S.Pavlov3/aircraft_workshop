package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.AreaSuperiorDto;
import nsu.DB.employees.mappers.AreaSuperiorsMapper;
import nsu.DB.employees.model.AreaSuperiors;
import nsu.DB.employees.filters.AreaSuperiorsFilter;
import nsu.DB.employees.services.AreaSuperiorsService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+ "/areaSuperiors")
public class AreaSuperiorsController extends AbstractCrudController<AreaSuperiors, AreaSuperiorDto> {

    public AreaSuperiorsController(AreaSuperiorsService service, AreaSuperiorsMapper mapper) {
        super(service, mapper, "AreaSuperiors");
    }


 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(AreaSuperiorsFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/

}
