package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.TestersDto;
import nsu.DB.employees.mappers.TestersMapper;
import nsu.DB.employees.model.Testers;
import nsu.DB.employees.services.TestersService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(AppPath.PATH_V1+"/testers")
public class TestersController extends AbstractCrudController<Testers, TestersDto> {

    public TestersController(TestersService service, TestersMapper mapper) {
        super(service, mapper, "Testers");
    }


/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(TestersFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     * Запрос 12:
     * Список испытателей, участвоваших в сбореке изделия, изделий и в некоторой лаборатории за
     * определенный промежуток времени
     */
    @GetMapping("by-lab-and-product-and-product-testing-date")
    public PageDto<?> getByLabAndProductAndProductTestingDate(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date startDate,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date endDate,
            @RequestParam
            List<Integer> productIds,
            @RequestParam
            Integer labId, FindRequestParameters parameters){
        Page<Testers> page = ((TestersService)getService()).
                findByLabAndProductAndProductTestingDate(startDate, endDate, productIds, labId, parameters);
        return getMapper().toPageDto(page);
    }
}
