package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.WorkersDto;
import nsu.DB.employees.mappers.WorkersMapper;
import nsu.DB.employees.model.Workers;
import nsu.DB.employees.services.WorkersService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/workers")
public class WorkersController extends AbstractCrudController<Workers, WorkersDto> {

    public WorkersController(WorkersService service, WorkersMapper mapper) {
        super(service, mapper, "Workers");
    }

 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(WorkersFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/

    @GetMapping("by-workshop")
    public PageDto<?> getByWorkshop(Integer workshopId, FindRequestParameters parameters){
        Page<Workers> page = ((WorkersService)getService()).findByWorkshop(workshopId, parameters);
        return getMapper().toPageDto(page);
    }
}
