package nsu.DB.employees.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.employees.dto.EngineersDto;
import nsu.DB.employees.mappers.EngineersMapper;
import nsu.DB.employees.model.Engineers;
import nsu.DB.employees.services.EngineersService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/engineers")
public class EngineersController extends AbstractCrudController<Engineers, EngineersDto> {

    public EngineersController(EngineersService service, EngineersMapper mapper) {
        super(service, mapper, "Engineers");
    }

 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(EngineersFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/

    @GetMapping("by-workshop")
    public PageDto<?> getByWorkshop(Integer workshopId, FindRequestParameters parameters){
        Page<Engineers> page = ((EngineersService)getService()).findByWorkshop(workshopId, parameters);
        return getMapper().toPageDto(page);
    }
}
