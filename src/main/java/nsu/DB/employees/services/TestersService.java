package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.Testers;
import nsu.DB.employees.repositories.TestersRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TestersService extends AbstractCrudService<Testers> {
    public TestersService(TestersRepository repository){ this.setRepository(repository);}

    public Page<Testers> findByLabAndProductAndProductTestingDate(
            Date startDate, Date endDate, List<Integer> productIds, Integer labId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((TestersRepository)getRepository()).
                findByLabAndProductAndProductTestingDate(startDate, endDate, productIds, labId, pageable);
    }
}
