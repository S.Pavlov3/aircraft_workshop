package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.AreaSuperiors;
import nsu.DB.employees.repositories.AreaSuperiorsRepository;
import org.springframework.stereotype.Service;

@Service
public class AreaSuperiorsService extends AbstractCrudService<AreaSuperiors> {
    public AreaSuperiorsService(AreaSuperiorsRepository repository){ this.setRepository(repository);}
}
