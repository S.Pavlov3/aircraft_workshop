package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.Workers;
import nsu.DB.employees.repositories.WorkersRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class WorkersService extends AbstractCrudService<Workers> {
    public WorkersService(WorkersRepository repository){ this.setRepository(repository);}

    public Page<Workers> findByWorkshop(Integer workshopId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((WorkersRepository)getRepository()).findByWorkshop(workshopId, pageable);
    }
}
