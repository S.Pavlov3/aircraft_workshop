package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.Brigades;
import nsu.DB.employees.repositories.BrigadesRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BrigadesService extends AbstractCrudService<Brigades> {
    public BrigadesService(BrigadesRepository repository){ this.setRepository(repository);}

    public Page<Brigades> findByAssembly(Integer productId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((BrigadesRepository)getRepository()).findByAssembly(productId, pageable);
    }

    public Page<Brigades> findByAreaOrWorkshop(
            Integer areasId, Integer workshopsId,FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((BrigadesRepository)getRepository()).findByAreaOrWorkshop(areasId,workshopsId, pageable);
    }
}
