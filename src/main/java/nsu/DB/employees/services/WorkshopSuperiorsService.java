package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.WorkshopSuperiors;
import nsu.DB.employees.repositories.WorkshopSuperiorsRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkshopSuperiorsService extends AbstractCrudService<WorkshopSuperiors> {
    public WorkshopSuperiorsService(WorkshopSuperiorsRepository repository){ this.setRepository(repository);}
}
