package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.Masters;
import nsu.DB.employees.repositories.MastersRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MastersService extends AbstractCrudService<Masters> {
    public MastersService(MastersRepository repository){ this.setRepository(repository);}

    public Page<Masters> findByAreaOrWorkshop(
            Integer areasId, Integer workshopsId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((MastersRepository)getRepository()).findByAreaOrWorkshop(areasId, workshopsId, pageable);
    }
}
