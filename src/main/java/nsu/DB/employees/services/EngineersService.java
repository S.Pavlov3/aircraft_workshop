package nsu.DB.employees.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.employees.model.Engineers;
import nsu.DB.employees.repositories.EngineersRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EngineersService extends AbstractCrudService<Engineers> {
    public EngineersService(EngineersRepository repository){ this.setRepository(repository);}

    public Page<Engineers> findByWorkshop(Integer workshopId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((EngineersRepository)getRepository()).findByWorkshop(workshopId, pageable);
    }
}
