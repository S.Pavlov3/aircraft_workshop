package nsu.DB.employees.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.employees.model.AreaSuperiors;

@Getter
@Setter
@NoArgsConstructor
public class AreaSuperiorDto extends Dto<AreaSuperiors> {

    private String name;
    private String surname;
    private String middleName;
    private String profession;
    private Integer areaId;
    
}
