package nsu.DB.employees.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.employees.model.Testers;

@Getter
@Setter
@NoArgsConstructor
public class TestersDto extends Dto<Testers> {

    private String name;
    private String surname;
    private String middleName;
}
