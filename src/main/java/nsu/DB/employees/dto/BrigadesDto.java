package nsu.DB.employees.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.employees.model.Brigades;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class BrigadesDto extends Dto<Brigades> {

    private Integer areaId;

    private Integer brigadeLeaderId;
    private Set<Integer> workersIds;
}
