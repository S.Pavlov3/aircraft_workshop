package nsu.DB.products.filters;

import nsu.DB.products.model.Products;
import nsu.DB.utils.filters.Filter;

public class ProductsFilter implements Filter<Products> {
    public String type;
    public Products.ProductCondition productCondition;
    public String attributes;
}
