package nsu.DB.products.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.products.model.Assembly;
import nsu.DB.products.repositories.AssemblyRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AssemblyService extends AbstractCrudService<Assembly> {
    public AssemblyService(AssemblyRepository repository){ this.setRepository(repository);}

    public Page<Assembly> findByProduct(Integer productId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((AssemblyRepository)getRepository()).findByProduct(productId, pageable);
    }
}
