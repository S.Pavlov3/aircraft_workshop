package nsu.DB.products.services;

import nsu.DB.abstracts.services.AbstractCrudService;
import nsu.DB.products.model.Products;
import nsu.DB.products.repositories.ProductsRepository;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ProductsService extends AbstractCrudService<Products> {
    public ProductsService(ProductsRepository repository){ this.setRepository(repository);}

    public Page<Products> findByLabAndProductTypeAndProductTestingDate(
            Date startDate, Date endDate,String type,Integer labId, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((ProductsRepository)getRepository()).
                findByLabAndProductTypeAndProductTestingDate(startDate,endDate,type,labId, pageable);
    }

    public Page<Products> findByTypeAndAreaOrWorkshopAtCurrentDate(
            Date currentDate,Integer areaId, Integer workshopId, String type, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((ProductsRepository)getRepository()).
                findByTypeAndAreaOrWorkshopAtCurrentDate(currentDate,areaId,workshopId,type, pageable);
    }

    public Page<Products> findByTypeAndWorkshop(Integer workshopId, String type, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((ProductsRepository)getRepository()).findByTypeAndWorkshop(workshopId, type, pageable);
    }

    public Page<Products> findByWorkshopAndProductTypeAtCurrentDate(
            Integer workshopId,Integer areaId,String type, Date currentDate, FindRequestParameters parameters){
        Pageable pageable = getPageable(parameters);
        return ((ProductsRepository)getRepository()).
                findByWorkshopAndProductTypeAtCurrentDate(workshopId,areaId,type, currentDate, pageable);
    }
}
