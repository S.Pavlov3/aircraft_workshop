package nsu.DB.products.repositories;

import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.products.model.Assembly;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AssemblyRepository extends JpaFilterRepository<Assembly,Integer> {

    /*@Query(
            "select a" +
                    "from assembly" +
                    "where :#{#filter.productId} is null or a.product.id = :#{#filter.productId}" +
                    "and :#{#filter.areaId} is null or a.area.id = :#{#filter.areaId}" +
                    "and :#{#filter.brigadeId} is null or a.brigade.id = :#{#filter.brigadeId}"
    )
    @Override
    Page<Assembly> findAllByFilter(@Param("filter")Filter<Assembly> filter, Pageable pageable);
*/

    /**
     * Запрос 5:
     * Перечень работ, которые проходило изделие
     */
    @Query(
            "select a " +
                    "from Assembly a " +
                    "where a.product.id = :productId"
    )
    public Page<Assembly> findByProduct(Integer productId, Pageable pageable);
}

