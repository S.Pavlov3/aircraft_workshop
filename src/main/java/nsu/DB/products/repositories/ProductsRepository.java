package nsu.DB.products.repositories;

import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.products.model.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ProductsRepository extends JpaFilterRepository<Products,Integer> {

   /* @Query(
            "select p" +
                    "from products" +
                    "when :#{#filter.type} is null or p.type = :#{#filter.type}" +
                    "and :#{#filter.productCondition} is null or p.productCondition = :#{#filter.productCondition}" +
                    "and :#{#filter.attributes} is null or p.attributes = :#{#filter.attributes}"
    )
    @Override
    Page<Products> findAllByFilter(@Param("filter") Filter<Products> filter, Pageable pageable);
*/

 /**
  *Перечень изделий отдельной категории, проходивших испытание в указанное время
  *      * в определенной лаборатории
  */
    @Query(
            "select p " +
                    "from Products p right join ProductTesting pt on pt.product = p " +
                    "where pt.startDate >= :startDate " +
                    "and pt.endDate <= :endDate " +
                    "and p.type = :type " +
                    "and pt.labEquipment.lab.id = :labId"
    )
    public Page<Products> findByLabAndProductTypeAndProductTestingDate(
            Date startDate, Date endDate,String type,Integer labId, Pageable pageable);

 /**
  *Перечень изделий, собираемых в данный момент участком, цехом, преприятием
  */
    @Query(
            "select p " +
                    "from Products p right join Assembly a on a.product = p " +
                    "where a.startDate >= :currentDate and a.endDate <= :currentDate " +
                    "and (a.area.id = :areaId or a.area.workshop.id = :workshopId) " +
                    "and p.type = :type "
    )
    public Page<Products> findByTypeAndAreaOrWorkshopAtCurrentDate(
            Date currentDate,Integer areaId, Integer workshopId, String type, Pageable pageable);

 /**
  *Запрос 1)
  * Перечень изделий отдельной категории и в целом, собираемых указанным цехом, предприятием
  */
    @Query(
            "select p " +
                    "from Products p right join Assembly a on a.product = p " +
                    "where (:type is null or p.type = :type  ) " +
                    "and (a.area.workshop.id = :workshopId or :workshopId is null)"
    )
    public Page<Products> findByTypeAndWorkshop(Integer workshopId, String type, Pageable pageable );

 /**
  *Число и перечень изделий отдельной категории, собираемых цехом, участком в данный момент
  */
    @Query(
            "select p " +
                    "from Products p right join Assembly a on a.product = p " +
                    "where (a.area.workshop.id = :workshopId or a.area.id = :areaId) " +
                    "and a.startDate >= :currentDate and a.endDate <= :currentDate " +
                    "and p.type = :type "
    )
    public Page<Products> findByWorkshopAndProductTypeAtCurrentDate(
            Integer workshopId,Integer areaId,String type, Date currentDate, Pageable pageable);

}

