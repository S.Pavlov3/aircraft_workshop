package nsu.DB.products.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.products.model.Products;

@Getter
@Setter
@NoArgsConstructor
public class ProductsDto extends Dto<Products> {

    private String type;
    private Products.ProductCondition productCondition;
    private String attributes;
}
