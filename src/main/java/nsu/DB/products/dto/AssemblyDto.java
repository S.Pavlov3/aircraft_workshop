package nsu.DB.products.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.products.model.Assembly;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AssemblyDto extends Dto<Assembly> {
    
    private Integer productId;
    private Integer areaId;
    private Integer brigadeId;
    
    private String assemblyName;

    private Date startDate;
    private Date endDate;
}
