package nsu.DB.products.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.companyStaff.services.AreasService;
import nsu.DB.employees.services.BrigadesService;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.products.dto.AssemblyDto;
import nsu.DB.products.model.Assembly;
import nsu.DB.products.services.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AssemblyMapper extends Mapper<Assembly, AssemblyDto> {

    private final AreasService areasService;

    private final ProductsService productsService;

    private final BrigadesService brigadesService;

    private final ModelMapper modelMapper;

    @Override
    public Assembly toEntity(AssemblyDto dto) throws NoDataFoundException {
        Assembly entity = modelMapper.map(dto, Assembly.class);
        if(dto.getAreaId() !=null &&
                dto.getAssemblyName()!= null &&
                dto.getBrigadeId()!=null &&
                dto.getProductId()!=null &&
                dto.getEndDate() != null &&
                dto.getStartDate()!=null  ) {
            entity.setArea(areasService.getEntity(dto.getAreaId()));
            entity.setAssemblyName(dto.getAssemblyName());
            entity.setBrigade(brigadesService.getEntity(dto.getBrigadeId()));
            entity.setProduct(productsService.getEntity(dto.getProductId()));
            entity.setEndDate(dto.getEndDate());
            entity.setStartDate(dto.getStartDate());
        }
        else{
            throw new NoDataFoundException("Something wrong with dto params");
        }
        return entity;
    }

    @Override
    public AssemblyDto toDto(Assembly entity) {
        AssemblyDto dto = modelMapper.map(entity,AssemblyDto.class);
        dto.setAreaId(entity.getArea() != null ? entity.getArea().getId() : null);
        dto.setAssemblyName(entity.getAssemblyName());
        dto.setBrigadeId(entity.getBrigade()!= null ? entity.getBrigade().getId() : null);
        dto.setProductId(entity.getProduct()!= null ? entity.getProduct().getId() : null);
        dto.setEndDate(entity.getEndDate());
        dto.setStartDate(entity.getStartDate());
        return dto;
    }
}
