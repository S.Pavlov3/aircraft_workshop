package nsu.DB.products.mappers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.products.dto.ProductsDto;
import nsu.DB.products.model.Products;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductsMapper extends Mapper<Products, ProductsDto> {

    private final ModelMapper modelMapper;

    @Override
    public Products toEntity(ProductsDto dto) throws NoDataFoundException {
        Products entity = modelMapper.map(dto, Products.class);
        entity.setType(dto.getType());
        entity.setAttributes(dto.getAttributes());
        if(dto.getProductCondition()!=null)
        entity.setProductCondition(dto.getProductCondition());
        else throw new NoDataFoundException("Wrong productCondition");
        return entity;
    }

    @Override
    public ProductsDto toDto(Products entity) {
        ProductsDto dto = modelMapper.map(entity,ProductsDto.class);
        dto.setType(entity.getType());
        dto.setProductCondition(entity.getProductCondition());
        dto.setAttributes(entity.getAttributes());
        return dto;
    }
}
