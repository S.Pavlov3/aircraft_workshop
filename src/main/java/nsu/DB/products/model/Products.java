package nsu.DB.products.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "Products")
public class Products extends Identifiable {

    public enum ProductCondition{
        ASSEMBLY,
        TESTING,
        DONE
    }

    private String type;
    private ProductCondition productCondition;
    private String attributes;
}
