package nsu.DB.products.model;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.companyStaff.model.Areas;
import nsu.DB.employees.model.Brigades;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "Assembly")
public class Assembly extends Identifiable {

    @ManyToOne
    @JoinColumn(name = "productId",referencedColumnName = "id")
    private Products product;

    @ManyToOne
    @JoinColumn(name = "areaId",referencedColumnName = "id")
    private Areas area;

    @ManyToOne
    @JoinColumn(name = "brigadeId",referencedColumnName = "id")
    private Brigades brigade;

    private String assemblyName;

    private Date startDate;

    private Date endDate;
}
