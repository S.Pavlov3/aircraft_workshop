package nsu.DB.products.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.products.dto.AssemblyDto;
import nsu.DB.products.mappers.AssemblyMapper;
import nsu.DB.products.model.Assembly;
import nsu.DB.products.services.AssemblyService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1+"/assembly")
public class AssemblyController extends AbstractCrudController<Assembly, AssemblyDto> {

    public AssemblyController(AssemblyService service, AssemblyMapper mapper) {
        super(service, mapper, "Assembly");
    }

/*    @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(AssemblyFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/
    /**
     * Запрос 5:
     * Перечень работ, которые проходило изделие
     */
    @GetMapping("by-product")
    public PageDto<?> getByProduct(Integer productId, FindRequestParameters parameters) {
        Page<Assembly> page = ((AssemblyService) getService()).findByProduct(productId, parameters);
        return getMapper().toPageDto(page);
    }
}
