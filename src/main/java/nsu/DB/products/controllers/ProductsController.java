package nsu.DB.products.controllers;

import nsu.DB.abstracts.controllers.AbstractCrudController;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.products.dto.ProductsDto;
import nsu.DB.products.mappers.ProductsMapper;
import nsu.DB.products.model.Products;
import nsu.DB.products.services.ProductsService;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(AppPath.PATH_V1+"/products")
public class ProductsController extends AbstractCrudController<Products, ProductsDto> {

    public ProductsController(ProductsService service, ProductsMapper mapper) {
        super(service, mapper, "Products");
    }

 /*   @GetMapping("/filter")
    PageDto<?> getAllEntitiesByFilter(ProductsFilter filter, FindRequestParameters params){
        return super.findAllByFilter(filter, params);
    }*/

    /**
     *Перечень изделий отдельной категории, проходивших испытание в указанное время
     * в определенной лаборатории
     */
    /**
     *НЕ ПРОТЕСТИРОВАННЫЙ КРУД С ДАТОЙ
     */

    @GetMapping("by-lab-and-productType-and-product-testing-date")
    public PageDto<?> getByLabAndProductTypeAndProductTestingDate(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date startDate,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date endDate, String type, Integer labId,FindRequestParameters parameters) {
        Page<Products> page = ((ProductsService) getService()).
                findByLabAndProductTypeAndProductTestingDate(startDate, endDate, type,labId, parameters);
     return getMapper().toPageDto(page);
 }
    /**
    *Перечень изделий, собираемых в данный момент участком, цехом, преприятием
    /**
     *НЕ ПРОТЕСТИРОВАННЫЙ КРУД С ДАТОЙ
     */
    @GetMapping("by-type-and-area-or-workshop-at-current-date")
    public PageDto<?> getByTypeAndAreaOrWorkshopAtCurrentDate(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date currentDate,
            @RequestParam(required = false)
            Integer areaId,
            @RequestParam(required = false)
            Integer workshopId,
            @RequestParam(required = false)
            String type, FindRequestParameters parameters) {
        Page<Products> page = ((ProductsService) getService()).
                findByTypeAndAreaOrWorkshopAtCurrentDate(currentDate,areaId,workshopId,type, parameters);
        return getMapper().toPageDto(page);
    }

    /**
     *Запрос 1)
     * Перечень изделий отдельной категории и в целом, собираемых указанным цехом, предприятием
     */
    @GetMapping("by-type-and-workshop")
    public PageDto<?> getByTypeAndWorkshop(
            Integer workshopId, String type, FindRequestParameters parameters) {
        Page<Products> page = ((ProductsService) getService()).
                findByTypeAndWorkshop(workshopId, type, parameters);
        return getMapper().toPageDto(page);
    }

    /**
     * Число и перечень изделий отдельной категории, собираемых цехом, участком в данный момент
     *НЕ ПРОТЕСТИРОВАННЫЙ КРУД С ДАТОЙ
     */
    @GetMapping("by-workshop-and-product-type-at-current-date")
    public PageDto<?> getByWorkshopAndProductTypeAtCurrentDate(
            Integer workshopId,
            Integer areaId,
            String type,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @RequestParam
            Date currentDate, FindRequestParameters parameters) {
        Page<Products> page = ((ProductsService) getService()).
                findByWorkshopAndProductTypeAtCurrentDate(workshopId,areaId,type,currentDate, parameters);
        return getMapper().toPageDto(page);
    }
}
