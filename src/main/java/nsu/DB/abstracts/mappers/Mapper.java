package nsu.DB.abstracts.mappers;

import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.exceptions.NoDataFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Интерфейс, который маппит из Entity типа E в дто типа D
 */
public abstract class Mapper<E, D> {

    @Autowired
    private ModelMapper modelMapper;

    public abstract E toEntity(D dto) throws NoDataFoundException;

    public abstract D toDto(E entity);

    public PageDto<?> toPageDto(Page<E> entityPage){
        Page dtoPage = new PageImpl(toDtos(entityPage.getContent()), entityPage.getPageable(), entityPage.getTotalElements());
        PageDto pageDto = modelMapper.map(dtoPage, PageDto.class);
        try{
            pageDto.setPageNumber(dtoPage.getPageable().getPageNumber());
        }
        catch(UnsupportedOperationException e){
            pageDto.setPageNumber(0);
        }
        return pageDto;
    }

    private List<D> toDtos(List<E> entities){
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }

}
