package nsu.DB.abstracts.controllers;


import nsu.DB.abstracts.dto.Dto;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.FindRequestParameters;
import nsu.DB.abstracts.model.Identifiable;


public interface CommonCrudController<E extends Identifiable, D extends Dto<E>> {

    public PageDto<?> getAllEntities(FindRequestParameters params);

    public void addEntity(D dto) throws NoDataFoundException;

    public void deleteEntity(int id) throws NoDataFoundException;

    public D getEntity(int id) throws NoDataFoundException;

    public void updateEntity(D dto) throws NoDataFoundException;


}
