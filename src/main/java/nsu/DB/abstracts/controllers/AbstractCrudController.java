package nsu.DB.abstracts.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nsu.DB.abstracts.dto.Dto;
import nsu.DB.abstracts.dto.PageDto;
import nsu.DB.abstracts.mappers.Mapper;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.abstracts.services.CommonCrudService;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.web.bind.annotation.*;

@Setter
@Getter
@Slf4j
public abstract class AbstractCrudController<E extends Identifiable, D extends Dto<E>>
        implements CommonCrudController<E, D>{

    private CommonCrudService<E> service;

    private Mapper<E,D> mapper;

    private String entityName;

    public AbstractCrudController(CommonCrudService<E> service, Mapper<E, D> mapper, String entityName) {
        this.service = service;
        this.mapper = mapper;
        this.entityName = entityName;
    }

    @GetMapping
    @Override
    public PageDto<?> getAllEntities(FindRequestParameters params) {
        log.info("All {}s were fetched",entityName);
        return mapper.toPageDto(service.getAllEntities(params));
    }

    @PostMapping
    @Override
    public void addEntity(@RequestBody D dto) throws NoDataFoundException {
        Identifiable entity = mapper.toEntity(dto);
        service.addEntity(mapper.toEntity(dto));
        log.info("{} was added",entityName);
    }


    @DeleteMapping("/{id}")
    @Override
    public void deleteEntity(@PathVariable int id) throws NoDataFoundException {
        service.deleteEntity(id);
        log.info("{} #'{}' was deleted",entityName, id);
    }

    @GetMapping("/{id}")
    @Override
    public D getEntity(@PathVariable int id) throws NoDataFoundException {
        log.info("{} #'{}' was queried",entityName,id);
        return mapper.toDto(service.getEntity(id));
    }

    @PutMapping
    @Override
    public void updateEntity(@RequestBody D dto) throws NoDataFoundException {
        service.updateEntity(mapper.toEntity(dto));
        log.info("{} '{}' was updated",entityName,dto.getId());
    }

/*    @PreAuthorize("hasAuthority('READ')")
    public PageDto<?> findAllByFilter(Filter<E> filter,
                                      FindRequestParameters requestParams){
        Page page = service.getAllEntitiesByFilter(filter,requestParams);
        log.info("{}s filter method was queried",entityName);
        return mapper.toPageDto(page);
    }*/

}
