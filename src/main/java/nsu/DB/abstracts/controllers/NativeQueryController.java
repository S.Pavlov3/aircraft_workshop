package nsu.DB.abstracts.controllers;

import lombok.RequiredArgsConstructor;
import nsu.DB.abstracts.dto.NativeQueryResultsDto;
import nsu.DB.abstracts.services.NativeQueryService;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.AppPath;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppPath.PATH_V1)
@RequiredArgsConstructor
public class NativeQueryController {

    private final NativeQueryService queryService;

    @GetMapping("/query")
    public NativeQueryResultsDto getQueryResults
            (@RequestParam String query, FindRequestParameters params) throws NoDataFoundException {
        return queryService.getQueryResults(query, params);
    }
}
