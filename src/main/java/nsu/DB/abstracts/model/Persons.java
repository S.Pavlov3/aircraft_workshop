package nsu.DB.abstracts.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "Persons")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persons extends Identifiable{

    private String name;
    private String surname;
    private String middleName;
    
}
