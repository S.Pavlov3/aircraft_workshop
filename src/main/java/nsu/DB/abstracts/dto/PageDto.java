package nsu.DB.abstracts.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PageDto <D>{
    private List<D> content;
    private int totalElements;
    private int pageNumber;
}
