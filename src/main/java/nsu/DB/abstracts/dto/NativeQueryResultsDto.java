package nsu.DB.abstracts.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class NativeQueryResultsDto {
    private int totalCount;
    private List<?> result;

    public NativeQueryResultsDto(int totalCount, List<?> result) {
        this.totalCount = totalCount;
        this.result = result;
    }
}
