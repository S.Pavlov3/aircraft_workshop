package nsu.DB.abstracts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface JpaFilterRepository<E, Id> extends JpaRepository<E, Id>, JpaSpecificationExecutor<E> {

   // public Page<E> findAllByFilter (Filter<E> filter, Pageable pageable);

}
