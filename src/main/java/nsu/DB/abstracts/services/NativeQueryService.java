package nsu.DB.abstracts.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nsu.DB.abstracts.dto.NativeQueryResultsDto;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.FindRequestParameters;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class NativeQueryService {

    private final EntityManager entityManager;

    private static final int DEFAULT_PAGE_SIZE = 500;

    public NativeQueryResultsDto getQueryResults(String queryString, FindRequestParameters params) throws NoDataFoundException {
        int page = getPage(params);
        int pageSize = getPageSize(params);
        int totalCount = 0;

        try {
            log.info("New sql query: '{}'", queryString);

            var query = entityManager
                    .createNativeQuery(queryString)
                    .setFirstResult(page * pageSize)
                    .setMaxResults(pageSize);

            if (query.getResultList().size() > 0) {
                totalCount = getTotalCount(queryString);
            }
            return new NativeQueryResultsDto(totalCount, query.getResultList());
        } catch (Exception e) {
            log.info("SQL query exception: {}", e.getLocalizedMessage());
            if (e.getCause() != null && e.getCause().getClass() == GenericJDBCException.class) {
                return new NativeQueryResultsDto(totalCount, new ArrayList<Object>());
            }
            throw new NoDataFoundException("No data");
        }

    }

    private int getPage(FindRequestParameters params) {
        return params.getPage() != 0 ? params.getPage() : 0;
    }

    private int getPageSize(FindRequestParameters params) {
        return params.getPageSize() != 0 ? params.getPageSize() : DEFAULT_PAGE_SIZE;
    }

    private Integer getTotalCount(String query) {
        List countArray = entityManager.
                createNativeQuery("SELECT count(*) from(" + query + ") AS QUERY").getResultList();
        if (countArray.size() > 0) {
            return ((BigInteger) countArray.get(0)).intValue();
        } else {
            return 0;
        }
    }


}
