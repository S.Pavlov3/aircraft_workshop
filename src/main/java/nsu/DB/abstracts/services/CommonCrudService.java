package nsu.DB.abstracts.services;

import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.FindRequestParameters;
import org.springframework.data.domain.Page;

public interface CommonCrudService<E> {

    public Page<E> getAllEntities(FindRequestParameters parameters);

//    public Page<E>getAllEntitiesByFilter(Filter<E> filter, FindRequestParameters parameters);

    public void addEntity(E entity);

    public void deleteEntity(int id) throws NoDataFoundException;

    public E getEntity(int id) throws NoDataFoundException;

    public void updateEntity(E entity) throws NoDataFoundException;

}
