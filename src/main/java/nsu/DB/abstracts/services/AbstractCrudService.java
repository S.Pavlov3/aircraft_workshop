package nsu.DB.abstracts.services;

import lombok.Getter;
import lombok.Setter;
import nsu.DB.abstracts.model.Identifiable;
import nsu.DB.abstracts.repositories.JpaFilterRepository;
import nsu.DB.exceptions.NoDataFoundException;
import nsu.DB.utils.FindRequestParameters;
import nsu.DB.utils.UtilFunctions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;

import static nsu.DB.utils.UtilFunctions.getSort;

public abstract class AbstractCrudService<E extends Identifiable> implements CommonCrudService<E> {

    @Getter
    @Setter
    private JpaFilterRepository<E,Integer> repository;

    private int defaultPageSize = 20;

    @Override
    public Page<E> getAllEntities(FindRequestParameters parameters) {
        Pageable pageable = getPageable(parameters);
        return repository.findAll(pageable);
    }

/*    @Override
    public Page<E> getAllEntitiesByFilter(Filter<E> filter, FindRequestParameters parameters) {
        Pageable pageable = getPageable(parameters);
        return repository.findAllByFilter(filter,pageable);
    }*/

    @Transactional
    @Override
    public void addEntity(E entity) {
        repository.save(entity);
    }

    @Override
    public void deleteEntity(int id) throws NoDataFoundException {
            if (repository.existsById(id)) {
                repository.deleteById(id);
            } else {
                throw new NoDataFoundException("Wrong id to delete");
            }
    }

    @Override
    public E getEntity(int id) throws NoDataFoundException {
        return repository.findById(id).orElseThrow(() -> new NoDataFoundException("Wrong id: $id"));
    }

    @Override
    public void updateEntity(E entity) throws NoDataFoundException {
            int id = entity.getId();

            if (repository.existsById(id)) {
                repository.save(entity);
            } else {
                throw new NoDataFoundException("Wrong id: $id");
            }
    }

    public Pageable getPageable(FindRequestParameters parameters){
        Sort sort = getSort(parameters);
        Pageable pageable = UtilFunctions.getPageable(parameters, sort);
        Integer intPageSize = defaultPageSize;
        if(pageable.isPaged())return pageable;
        return PageRequest.of(0,intPageSize,sort);
    }

}
